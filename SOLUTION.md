## Issue summary

- Employees get "stuck" in assignments, or can't be assigned to begin with.
- Same employee can be actively assigned multiple times.
- Number of assignments don't always match the number of product marketing points.
- Employees appear to be assigned but points aren't allocated correctly.
- Product marketing points can sometimes be negative.


## The Problem

The main issue seems to be that the `assignment_(add|remove)` event handlers aren't idempotent. Calling either of these more than once with the same parameters should result in the same state; the employee should be added or removed once, and any additional side-effects (e.g. increments) applied only once.

In the code, add/remove actions proceed without taking into account the current state of the data. This allows for some scenarios that can create invalid states. For example:

1. Button-mashing which results in multiple requests for the same action from the same player (if the requests are rapid enough, which depends on network latency).
```
  CLIENT         |  BACKEND              |  DB  
---[p1_click]--->|->[assign_jørgen_tv]-->|-->[write]
-----[p1_click]->|->[assign_jørgen_tv]-->|-->[write]

jørgen is assigned to 'tv' twice, by the same player
```
2. Two or more players could conceivably perform the same action at approximately the same time, resulting in duplication or some other conflicting state.
```
  CLIENT         |  BACKEND                       |  DB 
---[p1_click]--->|--->[assign_jørgen_tv]--------->|-->[write]
---[p2_click]--->|--->[assign_jørgen_newspaper]-->|-->[write]

jørgen is assigned to 'tv' and 'newspaper', by different players
```


The solutions below would improve the application behaviour in these and similar scenarios.

## Solution

Perform some basic checks when performing Create/Delete actions. Two changes you should make on the server:

- **Check for an `Assignment` with `employeeId` before creating a new record (and bail if it exists)**:
There's no unique constraint on the `employeeId` field, so all attempts to insert a record with a specific employeeId will succeed. The check will significantly reduce the possibility of accidentally (or intentionally) assigning the same employee multiple times.

- **Check the deletion result to ensure a record was deleted (bail if not)**: At the moment, the code tries to delete the record and then performs the negative increment regardless of the outcome. Looking at the deleted record count will allow you to determine if further actions should be taken (like decrementing the `marketingPoints` value).

The above changes should prevent:
  - duplicate records from being created
  - product marketing points from going out of sync with assigned employees
  - product marketing points from being negative


## Further Thoughts

### The Edge
The check above won't completely eliminate the possibility of multiple assignment, because it's still possible that two requests for the same action could occur so close together that they both read the same state (e.g. Assignment with employeeId doesn't exist) and perform the action based on that, resulting in a duplicate action. Some options:

- **Unique constraint on `Assignment.employeeId`**: Whether or not this is a possibility depends on if/how `Assignment` is used in other simulations (maybe multitasking is allowed).

- **Queue (with concurrency of 1)**: If the simulation had it's own queue, certain actions (create/delete) could be funneled through it. In this case, no matter how many requests come in simultaneously, they'd still only be processed one after another, meaning the next would see the state as updated by the last and avoid performing any duplicative actions.

### Feedback to the player

It would be useful to provide more feedback to the player when they take action in the game (e.g. simple animation to show the request is being processed, "toast" messages). The backend could make use of Socket.io's `ack` to send basic success/failure info when create/update/delete actions are performed so that the client can act accordingly.

### Marketing Points

If my understanding is correct, the data models are shared between simulations, but not the data. If this is the case, it seems that it would be safe in this simulation to calculate the `Product.marketingPoints` value by retrieving the count of the relevant assignments. It does mean an additional query to the database, but it could guarantee that the value is correct and would prevent incorrect/missing logic from having an impact on that particular value, as is the case in the current code. It would also require no change to `Product` and could still remain the source of truth, so shouldn't affect any other simulations.

## Misc

I couldn't help but notice there are no tests. I would definitely break out the socket event handlers into external functions and at least add some unit tests around the above issues.